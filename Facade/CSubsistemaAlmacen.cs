﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Subsistema
{
    class CSubsistemaAlmacen  // Esta clase representa a un SubsistemaAlmacen
    {
        private int cantidad;
        public CSubsistemaAlmacen()
        {
            cantidad = 3; //este es un constructor de valor de inicializacion
        }
        public bool SacarAlmacen() //Es un metodo
        {
            if (cantidad > 0) //if si es mayor a 0 regresara enviara el mensaje ue esta a continuacion
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Peoducto listo para enviar");
                cantidad--;
                return true;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red; //si es falso devolvera este mensaje
                Console.WriteLine("Producto no se esperaba");
                return false;

            }


        }
    }
}
