﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Subsistema        //Adicional  esta linea de Codigo--Console.ForegroundColor = ConsoleColor.Cyan;--- lo que hace es darle estilo al mostrar un 
{                           //menaje en cosola el cual cambia el color de la letra el cual si es un erro te saldra con rojo  asi haciendolo mas llamativo y facil de enteder 
    class CSistemaCompras // Esta clase representa a un subsistema
    {
        public bool Compar() //Se crea un metodo el cual representa una operacion
        {
            string dato = "";
            Console.ForegroundColor = ConsoleColor.White; //Console.ForegroundColor es para poder ponerle colo a las lineas que mostrara en 
            Console.WriteLine("Introducir Numero de Tarjeta"); //le permite al usuario que ingrese el numero de la tarjeta
            dato = Console.ReadLine();

            if (dato == "12345")  //este if valida si el codigo es correcto, si el codigo es correcto procedera siguiente
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Pago Aceptado"); //Si es dato es correcto saldra este mensaje
                return true;
            }

            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Pago rechazado"); //si el dato ingresdo el incorrecto devolvera este mensaje
                return false;
            }
        }
    }
}
