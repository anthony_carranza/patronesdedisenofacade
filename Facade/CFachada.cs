﻿using Subsistema;
using System;
using System.Collections.Generic;
using System.Text;

namespace Facade
{
    class CFachada
    {
        private CSistemaCompras compras = new CSistemaCompras(); //Instancia de Csistema compra 
        private CSubsistemaenvio envio = new CSubsistemaenvio(); //Instancia de Subsotemas envio 
        private CSubsistemaAlmacen almacen = new CSubsistemaAlmacen(); //Instancia de CSubsitema Almacen
        public void Comprar()                   //se crea un metodo el cual el cual hace mas sencillo al cliente  
        {                                      //donde el usrio solo dira comprar y se reliazarn las operacion 
            if (compras.Compar())              //Esta es la funcion que hace el patron de diseño FACADE el reduce la complejidad
            {                                   // de estar ingresando a todo esos subsitemas
                if (almacen.SacarAlmacen())
                {
                    envio.EnviarPedido();
                }
            }
        }
    }
}
