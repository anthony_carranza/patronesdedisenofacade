﻿using System;
using Subsistema;

namespace Facade
{
    class Program
    {
        
        static void Main(string[] args)
        {
            CFachada fachada = new CFachada();

            fachada.Comprar();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("...........");
        }
    }
}
